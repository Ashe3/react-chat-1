import React, { useState } from 'react';

import './App.css';
import Header from './components/Header';
import MsgList from './components/Messages';
import SendMessage from './components/SendMessage';

function Chat({ url }) {
  const [messages, setMessages] = useState([]);
  const [participants, setParticipants] = useState(10);
  const [messagesCount, setmMessagesCount] = useState(0);
  const [lastMsg, setlastMsg] = useState('');

  const handleSend = (value) => {
    setMessages([...messages, value]);
  };

  const handleRemove = (messageId) => {
    const msgIndex = findMessage(messageId);
    if (msgIndex >= 0) {
      messages.splice(msgIndex, 1);
      setMessages([...messages]);
    }
  };

  const handleEdit = (id, messageData) => {
    const index = findMessage(id);
    if (index >= 0) {
      messages[index] = { ...messages[index], ...messageData };
      setMessages([...messages]);
    }
  };

  const findMessage = (msgId) => {
    return messages.reduce((acc, { id }, index) => {
      if (id === msgId) acc = index;
      return acc;
    }, -1);
  };

  const getMessagesData = (messageCount, particip, messageData) => {
    setParticipants(particip);
    setmMessagesCount(messageCount);
    setlastMsg(messageData);
  };

  return (
    <div className='App chat'>
      <Header
        participants={participants}
        messages={messagesCount}
        lastMsg={lastMsg}
      />
      <MsgList
        messages={messages}
        handleRemove={handleRemove}
        handleEdit={handleEdit}
        getMessagesData={getMessagesData}
        setParticipants={setParticipants}
        url={url}
      />
      <SendMessage handleSend={handleSend} />
    </div>
  );
}

export default Chat;
