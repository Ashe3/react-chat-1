import React, { useState } from 'react';

import Editor from './Editor';

const UserMessage = (props) => {

  const [isEdit, setEdit] = useState(false);

  const normalizeDate = (date) => {
    const jsDate = new Date(date);
    return `${jsDate.getHours()}:${jsDate.getMinutes()}`
  };

  const editMsg = () => {
    setEdit(!isEdit);
  }

  const removeMsg = () => {
    props.handleRemove(props.id);
  }

  const saveMsg = (text) => {
    const newMessageData = {
      editedAt: new Date(Date.now()),
      text
    };

    props.handleEdit(props.id, newMessageData);
    setEdit(!isEdit)
  }

  const showMessage = () => {
    return (
      <div className='container'>
        <div className='edit'>
          <span className="message-edit material-icons" onClick={editMsg}>
            create
          </span>
          <span className="message-delete material-icons" onClick={removeMsg}>
            delete
          </span>
        </div>
        <div className='text'>
          <span className="message-text">{props.text}</span>
          <span className='message-time date'>{props.editedAt ? `Edited ${normalizeDate(props.editedAt)}` : normalizeDate(props.createdAt)}</span>
        </div>
      </div>
    )
  }

  return (
    <div className="message user-message own-message">
      {isEdit ? <Editor text={props.text} saveMsg={saveMsg} /> : showMessage()}
    </div>

  )
}

export default UserMessage;