import React, { useState } from 'react';

const Editor = (props) => {

  const [inputValue, setValue] = useState(props.text);

  return (
    <div className='editor'>
      <input type='text' value={inputValue} onChange={(e) => {setValue(e.target.value)}} autoFocus></input>
      <span className="material-icons" onClick={() => {props.saveMsg(inputValue)}}>
        save
      </span>
    </div>
  )
}

export default Editor;