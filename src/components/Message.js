import React, { useState } from 'react';

const MsgBox = (props) => {

  const [red, setRed] = useState(false);

  const normalizeDate = (date) => {
    const jsDate = new Date(date);
    return `${jsDate.getHours()}:${jsDate.getMinutes()}`
  };

  const likeMessage = () => {
    setRed(!red)
  };

  return (
    <div className='message'>
      <img src={props.avatar} alt="user avatar" className="message-user-avatar"/>
      <div className='text'>
        <span className='message-user-name name'>{props.username}</span>
        <span className="message-text">{props.text}</span>
        <span className='message-time date'>{props.editedAt.length > 0 ? `Edited ${normalizeDate(props.editedAt)}` : normalizeDate(props.createdAt)}</span>
      </div>
      <div id='like' className={red ? 'red message-liked' : 'white message-like'} onClick={likeMessage}>
        <span className="material-icons">
          favorite
        </span>
      </div>
    </div>
  );
}

export default MsgBox;