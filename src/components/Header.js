import React from 'react';

const Header = (props) => {
  return (
    <div className="header">
      <span className="header-title">My Chat</span>
      <span><span className="header-users-count">{props.participants}</span> participants</span>
      <span><span className="header-messages-count">{props.messages}</span> messages</span>
      <span>Last Message at <span className="header-last-message-date">{props.lastMsg}</span></span>
    </div>
  );
};

export default Header;