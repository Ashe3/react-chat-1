import React from 'react';

import MsgBox from './Message';
import UserMessage from './UserMessage';
import Spinner from './Spinner';

import { getMessageList } from '../helpers/apiHelper';

class MsgList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      messages: [],
    };
  }

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: 'smooth' });
  };

  async componentDidMount() {
    const ms = await getMessageList(this.props.url);
    this.setState({
      messages: ms,
      isLoading: false,
    });
    this.scrollToBottom();
  }

  componentDidUpdate(prev) {
    const fullMessagesArray = this.getMessages([
      ...this.state.messages,
      ...this.props.messages,
    ]);
    const participants = new Set(fullMessagesArray.map(({ userId }) => userId))
      .size;
    const messagesCount = fullMessagesArray.length;
    const lastDate = new Date(fullMessagesArray[messagesCount - 1]?.createdAt);
    const lastMessageMonth =
      lastDate.getMonth() + 1 < 10
        ? `0${lastDate.getMonth() + 1}`
        : lastDate.getMonth() + 1;
    const lastMessageDay =
      lastDate.getDate() < 10 ? `0${lastDate.getDate()}` : lastDate.getDate();
    const dateString = `${lastMessageDay}.${lastMessageMonth}.${lastDate.getFullYear()} ${lastDate.getHours()}:${lastDate.getMinutes()}`;
    this.props.getMessagesData(messagesCount, participants, dateString);

    this.scrollToBottom();
  }

  getMessages(data) {
    const messages = data.sort(
      ({ createdAt: create1 }, { createdAt: create2 }) =>
        new Date(create1) - new Date(create2)
    );
    return messages;
  }

  render() {
    const fullMessagesArray = this.getMessages([
      ...this.state.messages,
      ...this.props.messages,
    ]);

    const ms = fullMessagesArray.map(
      ({ id, userId, avatar, user, text, createdAt, editedAt }, idx) => {
        switch (userId) {
          case 7:
            return (
              <UserMessage
                key={id + idx}
                id={id}
                userId={userId}
                text={text}
                createdAt={createdAt}
                editedAt={editedAt}
                handleRemove={this.props.handleRemove}
                handleEdit={this.props.handleEdit}
              />
            );
          default:
            return (
              <MsgBox
                key={id + idx}
                id={id}
                avatar={avatar}
                userId={userId}
                username={user}
                text={text}
                createdAt={createdAt}
                editedAt={editedAt}
              />
            );
        }
      }
    );

    return (
      <div className='message-list'>
        {this.state.isLoading ? <Spinner /> : ms}
        <div
          style={{ float: 'left', clear: 'both', opacity: 0 }}
          ref={(el) => {
            this.messagesEnd = el;
          }}
        ></div>
      </div>
    );
  }
}

export default MsgList;
