import React from 'react';

class SendMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    }
    this.onChange = this.onChange.bind(this);
    this.onSend = this.onSend.bind(this);
    this.onSendEnter = this.onSendEnter.bind(this);
  }

  onChange(event) {
    const { value } = event.target;
    this.setState({
      value
    });
  }

  onSend() {
    const messageDetails = {
      id: Math.floor(Math.random() * Math.floor(100)),
      userId: 7,
      text: this.state.value,
      createdAt: new Date(Date.now()),
      editedAt: ''
    }
    this.props.handleSend(messageDetails);
    this.setState({
      value: ''
    })
  }

  onSendEnter(event) {
    if (event.key === 'Enter') {
      this.onSend();
    }
  }

  render() {
    return (
      <div className='message-input send'>
        <input type='text' className='send-field message-input-text' value={this.state.value} onChange={this.onChange} onKeyPress={this.onSendEnter}/>
        <div className='sendBtn center message-input-button' onClick={this.onSend} >Send</div>
      </div>
    );
  }
}

export default SendMessage;